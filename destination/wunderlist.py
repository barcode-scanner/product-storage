from . import wunderlistconfig
import wunderpy2

class Wunderlist:

	access_token = wunderlistconfig.access_token
	client_id = wunderlistconfig.client_id
	client = None

	def __init__(self):
		api = wunderpy2.WunderApi()
		self.client = api.get_client(self.access_token, self.client_id)

	def getLists(self):
		return self.client.get_lists()

	def getShoppingList(self):
		lists = self.getLists()
		return self.findShoppingList(lists)

	def findShoppingList(self, lists):
		result = [element for element in lists if element['title'] == 'shopping']
		if not result:
			return None
		else:
			return result[0]

	def createList(self, title):
		return self.client.create_list(title)

	def addProductToList(self, listId, productName):
		return self.client.create_task(listId, productName)