#import destination.wunderlist
import sys
sys.path.append('..')
from barcode_lookup.engine import colruyt

def addProductToShoppingList(product):
	wunderlist = destination.wunderlist.Wunderlist()
	shoppingList = wunderlist.getShoppingList()
	if not shoppingList:
		shoppingList = wunderlist.createList('shopping')
	wunderlist.addProductToList(shoppingList['id'], product.name)

def addProductToColruytBasket(id, quantity, weightCode):
	api = colruyt.ColruytAPI()
	api.add(id, quantity, weightCode)
	api.logout()
	del(api)
	
def showColruytBasket():
	api = colruyt.ColruytAPI()
	basket = api.show_basket()
	basket_content = u''
	for category in basket['data']['articles']:
		basket_content += category['colruyt.cogomw.bo.RestTreeBranch']['description'] + '\n'
		for article in category['list']:
			basket_content += '  %s - %s : %s stuks\n' %(article['brand'], article['description'], article['quantity'])
	basket_content += u'----------------------\n'
	basket_content += u'subtotal: %s euro\n' %(basket['data']['subTotal'])
	basket_content += u'service cost: %s euro\n' %(basket['data']['serviceCost'])
	basket_content += u'total: %s euro\n' %(basket['data']['total'])
	basket_content += u'-----------------------\n'
	print basket_content
	api.logout()
	del(api)