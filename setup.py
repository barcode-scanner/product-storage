#!/usr/bin/env python

from distutils.core import setup

setup(name='Product Storage',
      version='0.1',
      description='Takes a product (from the barcode-lookup domain) as a parameter and stores the product to various destinations',
      author='Jean-Christophe Raad & Yves Hulet',
      url='https://gitlab.com/barcode-scanner/product-storage',
     )